#! /bin/bash

config() {
    files=(   '.bashrc'  '.vimrc' '.inputrc' ) # 
    paths=(    $HOME      $HOME    $HOME     ) # 
    include=( 'source'   'source' '$include' )

    for i in "${!files[@]}"; do
        file=${files[$i]}
        path=${paths[$i]}
        inc=${include[$i]}
        [ -f $file ] && ln -s $PWD/$file "$path/$file.piti" \
            && [[ -z $(cat $path/$file | grep "$inc $path/$file.piti") ]] \
            && echo -e "\n$inc $path/$file.piti" >> $path/$file
    done
}

xinitrc() {
    sudo cp $PWD/xsession/xinitrc.desktop /usr/share/xsessions/
    sudo cp $PWD/xsession/xinitrcsession-helper /usr/bin/
}

# Check if the function exists (bash specific)
if declare -f "$1" > /dev/null
then
  # call arguments verbatim
  "$@"
else
  # Show a helpful error
  echo "'$1' is not a known function name" >&2
  exit 1
fi

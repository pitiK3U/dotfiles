" Disable arrow keys to go full vim
map <Up> <Nop>
map <Down> <Nop>
map <Left> <Nop>
map <Right> <Nop>

nmap <Up> <Nop>
nmap <Down> <Nop>
nmap <Left> <Nop>
nmap <Right> <Nop>

" Map the leader key to Space and local leader to Comma
let g:mapleader = "\<Space>"
let g:maplocalleader = ','
" nnoremap <silent> <leader>      :<c-u>WhichKey '<Space>'<CR>
" nnoremap <silent> <localleader> :<c-u>WhichKey  ','<CR>

" Set numbering
set number
set relativenumber

" Enable syntax highlight
filetype plugin indent on
syntax on

" Set auto reload on file change
set autoread

" Enable mouse use
set mouse=a

" See command live preview in different buffer
set inccommand=split

" rustfmt on write using autoformat
" autocmd BufWrite * :Autoformat
let g:autofmt_autosave = 1

" Incrementally search while typing
set incsearch
" Use smart case for searching
set ignorecase
set smartcase
" Highlight searches
set hlsearch
"Use <C-L> to clear the highlighting of :set hlsearch.
if maparg('<C-L>', 'n') ==# ''
    nnoremap <silent> <C-L> :nohlsearch<C-R>=has('diff')?'<Bar>diffupdate':''<CR><CR><C-L>
endif

" Deafult tab size to 4 spaces
set tabstop=4
set shiftwidth=4

" Tell Vim which characters to show for expanded TABs,
" trailing whitespace, and end-of-lines. VERY useful!
if &listchars ==# 'eol:$'
  set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
endif
set list                " Show problematic characters.

" Also highlight all tabs and trailing whitespace characters.
highlight ExtraWhitespace ctermbg=darkgreen guibg=darkgreen
match ExtraWhitespace /\s\+$\|\t/

"=============================================================

" Plugins
call plug#begin()
" Function signature in cmd line, MOST AWESOME PLUGIN
Plug 'Shougo/echodoc.vim'

" Collection of common configurations for the Nvim LSP client
Plug 'neovim/nvim-lspconfig'

" Nvim completion manager
Plug 'hrsh7th/nvim-cmp'

" Snippet engine to handle LSP snippets
Plug 'hrsh7th/vim-vsnip'
Plug 'hrsh7th/cmp-nvim-lsp'

" Other usefull completion sources
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-buffer'

" Extensions to built-in LSP, for example, providing type inlay hints
Plug 'nvim-lua/lsp_extensions.nvim'

" Autocompletion framework for built-in LSP
" Plug 'nvim-lua/completion-nvim'

" Rust better lsp
Plug 'simrat39/rust-tools.nvim'

" Rust toml syntax
Plug 'cespare/vim-toml'

" Neovim haskell syntax
Plug 'neovimhaskell/haskell-vim'

" Optional dependencies
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'

" clang-tidy for nvim
" Plug 'emilienlemaire/clang-tidy.nvim'
Plug 'dense-analysis/ale'

" Nvim telescope
Plug 'nvim-telescope/telescope.nvim'

" Cheatsheet for commands
Plug 'sudormrfbin/cheatsheet.nvim'

" Fuzzy finder
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'

" For keybindings hints
" Plug 'liuchengxu/vim-which-key'
Plug 'folke/which-key.nvim'

" For easier navigation
" Plug 'easymotion/vim-easymotion'
Plug 'phaazon/hop.nvim'

" Markdown renderer (for lsp hover)
Plug 'plasticboy/vim-markdown'

" Bracket auto pairing
Plug 'windwp/nvim-autopairs'
" For creating brackets
Plug 'tpope/vim-surround'
" Rainbow brackets to recognize them
Plug 'frazrepo/vim-rainbow'

" Better syntax highlighting
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}  " We recommend updating the parsers on update

" Tree file system explorer
" Old one = Plug 'scrooloose/nerdtree'
" for file icons
Plug 'kyazdani42/nvim-web-devicons'
Plug 'kyazdani42/nvim-tree.lua'

" Git plugin
Plug 'tpope/vim-fugitive'

" For outline using ctags
Plug 'majutsushi/tagbar'

" Tab Bar
Plug 'akinsho/bufferline.nvim'
" Botom status bar
Plug 'itchyny/lightline.vim'
" Theme for status bar
Plug 'mike-hearn/base16-vim-lightline'
" Nvim colorscheme
Plug 'chriskempson/base16-vim'

call plug#end()

"=============================================================

" Plugin settings

" To use echodoc, you must increase 'cmdheight' value.
let g:echodoc_enable_at_startup = 1
" Use neovim's virtual text
"let g:echodoc#type = 'floating'
" To use a custom highlight for the float window,
" change Pmenu to your highlight group
"highlight link EchoDocFloat Pmenu

" Set completeopt to have a better completion experience
" :help completeopt
" menuone: popup even when there's only one match
" noinsert: Do not insert text until a selection is made
" noselect: Do not select, force user to select one from the menu
set completeopt=menuone,noinsert,noselect

" Avoid showing extra messages when using completion
set shortmess+=c

" Configure LSP
" https://github.com/neovim/nvim-lspconfig#rust_analyzer
" lua <<EOF
" 
" -- nvim_lsp object
" local nvim_lsp = require'lspconfig'
" 
" -- function to attach completion when setting up lsp
" local on_attach = function(client)
"     require'completion'.on_attach(client)
" end
" 
" -- Enable rust_analyzer
" nvim_lsp.rust_analyzer.setup({ on_attach=on_attach })
" 
" -- Enable diagnostics
" vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
"   vim.lsp.diagnostic.on_publish_diagnostics, {
"     virtual_text = true,
"     signs = true,
"     update_in_insert = true,
"   }
" )
" EOF

lua <<EOF

local on_attach = function(client, bufnr)
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

    --Enable completion triggered by <c-x><c-o>
    buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')
end

local opts = {
    tools = { -- rust-tools options
        -- automatically set inlay hints (type hints)
        -- There is an issue due to which the hints are not applied on the first
        -- opened file. For now, write to the file to trigger a reapplication of
        -- the hints or just run :RustSetInlayHints.
        -- default: true
        autoSetHints = true,

        -- whether to show hover actions inside the hover window
        -- this overrides the default hover handler so something like lspsaga.nvim's hover would be overriden by this
        -- default: true
        hover_with_actions = true,

        -- These apply to the default RustRunnables command
        runnables = {
            -- whether to use telescope for selection menu or not
            -- default: true
            use_telescope = true

            -- rest of the opts are forwarded to telescope
        },

        -- These apply to the default RustSetInlayHints command
        inlay_hints = {
            -- wheter to show parameter hints with the inlay hints or not
            -- default: true
            show_parameter_hints = true,

            -- prefix for parameter hints
            -- default: "<-"
            parameter_hints_prefix = "<- ",

            -- prefix for all the other hints (type, chaining)
            -- default: "=>"
            other_hints_prefix = "=> ",

            -- whether to align to the lenght of the longest line in the file
            max_len_align = false,

            -- padding from the left if max_len_align is true
            max_len_align_padding = 1,

            -- whether to align to the extreme right or not
            right_align = false,

            -- padding from the right if right_align is true
            right_align_padding = 7
        },

        hover_actions = {
            -- the border that is used for the hover window
            -- see vim.api.nvim_open_win()
            border = {
                {"╭", "FloatBorder"}, {"─", "FloatBorder"},
                {"╮", "FloatBorder"}, {"│", "FloatBorder"},
                {"╯", "FloatBorder"}, {"─", "FloatBorder"},
                {"╰", "FloatBorder"}, {"│", "FloatBorder"}
            },

            -- whether the hover action window gets automatically focused
            -- default: false
            auto_focus = false
        }
    },

    -- all the opts to send to nvim-lspconfig
    -- these override the defaults set by rust-tools.nvim
    -- see https://github.com/neovim/nvim-lspconfig/blob/master/CONFIG.md#rust_analyzer
    server = {
		-- on_attach = on_attach,
		-- capabilities = capabilities,
	}, -- rust-analyzer options

}

require('rust-tools').setup(opts)

-- Command:
-- RustSetInlayHints
-- RustDisableInlayHints 
-- RustToggleInlayHints 

-- set inlay hints
-- require('rust-tools.inlay_hints').set_inlay_hints()
-- disable inlay hints
-- require('rust-tools.inlay_hints').disable_inlay_hints()
-- toggle inlay hints
-- require('rust-tools.inlay_hints').toggle_inlay_hints()

-- Command:
-- RustRunnables
-- require('rust-tools.runnables').runnables()

EOF


lua << EOF
-- Compe setup
-- require'compe'.setup {
--   enabled = true;
--   autocomplete = true;
--   debug = false;
--   min_length = 1;
--   preselect = 'enable';
--   throttle_time = 80;
--   source_timeout = 200;
--   incomplete_delay = 400;
--   max_abbr_width = 100;
--   max_kind_width = 100;
--   max_menu_width = 100;
--   documentation = true;
-- 
--   source = {
--     path = true;
--     buffer = true;
--     calc = true;
--     vsnip = true;
--     nvim_lsp = true;
--     nvim_lua = true;
--     spell = true;
--     tags = true;
--     snippets_nvim = true
--   };
-- }

-- Cmp setup
local has_words_before = function()
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

local feedkey = function(key, mode)
  vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes(key, true, true, true), mode, true)
end

local cmp = require'cmp'

  cmp.setup({
    completion = {
      autocomplete = true,
    },
    snippet = {
      expand = function(args)
        -- For `vsnip` user.
        vim.fn["vsnip#anonymous"](args.body)

        -- For `luasnip` user.
        -- require('luasnip').lsp_expand(args.body)

        -- For `ultisnips` user.
        -- vim.fn["UltiSnips#Anon"](args.body)
      end,
    },
    mapping = {
      ['<C-p>'] = cmp.mapping.select_prev_item(),
      ['<C-n>'] = cmp.mapping.select_next_item(),

      -- Add tab support
      ["<Tab>"] = cmp.mapping(function(fallback)
        if cmp.visible() then
          cmp.select_next_item()
        elseif vim.fn["vsnip#available"](1) == 1 then
          feedkey("<Plug>(vsnip-expand-or-jump)", "")
        elseif has_words_before() then
          cmp.complete()
        else
          fallback() -- The fallback function sends a already mapped key. In this case, it's probably `<Tab>`.
        end
      end, { "i", "s" }),

      ["<S-Tab>"] = cmp.mapping(function()
        if cmp.visible() then
          cmp.select_prev_item()
        elseif vim.fn["vsnip#jumpable"](-1) == 1 then
          feedkey("<Plug>(vsnip-jump-prev)", "")
        end
      end, { "i", "s" }),

      ['<C-d>'] = cmp.mapping.scroll_docs(-4),
      ['<C-f>'] = cmp.mapping.scroll_docs(4),
      ['<C-Space>'] = cmp.mapping.complete(),
      ['<C-e>'] = cmp.mapping.close(),
      ['<CR>'] = cmp.mapping.confirm({
        -- behavior = cmp.ConfirmBehavior.Replace,
        -- select = true,
      })
    },
    sources = {
      { name = 'path' },
      { name = 'nvim_lsp' },
      { name = 'nvim_lua' },
      { name = 'calc' },
      { name = 'tags' },
      { name = 'snippets_nvim' },
      { name = 'path' },
      { name = 'buffer' },

      -- For vsnip user.
      { name = 'vsnip' },

      -- For luasnip user.
      -- { name = 'luasnip' },

      -- For ultisnips user.
      -- { name = 'ultisnips' },
    },
    experimental = {
      ghost_text = true,
    },
  })

  -- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
  cmp.setup.cmdline('/', {
    sources = {
      { name = 'buffer' }
    }
  })

  -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
  -- cmp.setup.cmdline(':', {
  --   sources = cmp.config.sources({
  --     { name = 'cmdline' }
  --   }, {
  --     { name = 'path' }
  --   })
  -- })

local t = function(str)
  return vim.api.nvim_replace_termcodes(str, true, true, true)
end

local check_back_space = function()
    local col = vim.fn.col('.') - 1
    if col == 0 or vim.fn.getline('.'):sub(col, col):match('%s') then
        return true
    else
        return false
    end
end

-- Use (s-)tab to:
--- move to prev/next item in completion menuone
--- jump to prev/next snippet's placeholder
_G.tab_complete = function()
  if vim.fn.pumvisible() == 1 then
    return t "<C-n>"
  elseif check_back_space() then
    return t "<Tab>"
  else
    return vim.fn['cmp#complete']()
  end
end
_G.s_tab_complete = function()
  if vim.fn.pumvisible() == 1 then
    return t "<C-p>"
  else
    return t "<S-Tab>"
  end
end

vim.api.nvim_set_keymap("i", "<Tab>", "v:lua.tab_complete()", {expr = true})
vim.api.nvim_set_keymap("s", "<Tab>", "v:lua.tab_complete()", {expr = true})
vim.api.nvim_set_keymap("i", "<S-Tab>", "v:lua.s_tab_complete()", {expr = true})
vim.api.nvim_set_keymap("s", "<S-Tab>", "v:lua.s_tab_complete()", {expr = true})
EOF

let g:nvim_tree_show_icons = {
    \ 'git': 1,
    \ 'folders': 1,
    \ 'files': 0,
    \ 'folder_arrows': 1,
    \ }

lua << EOF

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

    --Enable completion triggered by <c-x><c-o>
    buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')
end

-- Add additional capabilities supported by nvim-cmp
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').update_capabilities(capabilities)

-- Use a loop to conveniently call 'setup' on multiple servers and
-- map buffer local keybindings when the language server attaches
local servers = { 'clangd', 'rust_analyzer', 'hls' }
for _, lsp in ipairs(servers) do
	-- nvim_lsp object
	local nvim_lsp = require'lspconfig'

	-- if (lsp == 'clangd') then
	-- 	require('clang-tidy').setup{
	-- 	checks = {
	-- 		'-*',
	-- 		'bugprone-*',
	-- 		'cppcoreguidelines-avoid-*',
	-- 		'readability-identifier-naming',
	-- 		'misc-assert-side-effect',
	-- 		'readability-container-size-empty-*',
	-- 		'modernize-*'
	-- 		},
	-- 	ignore_severity = {}
	-- 	}
	-- end

	nvim_lsp[lsp].setup({
		on_attach=on_attach,
		-- capabilities auto completes arguments I don't want that
		-- capabilities = capabilities,
	})
end

-- clang_tidy run
-- require('clang-tidy').run()

local lspconfig = require'lspconfig'
lspconfig.ccls.setup {
  init_options = {
    compilationDatabaseDirectory = "build";
    index = {
      threads = 0;
    };
    clang = {
      excludeArgs = { "-frounding-math"} ;
    };
  }
}

-- Enable diagnostics
vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
  vim.lsp.diagnostic.on_publish_diagnostics, {
    virtual_text = true,
    signs = true,
    update_in_insert = true,
  }
)

--Setup nvim treesitter
require'nvim-treesitter.configs'.setup {
  ensure_installed = "maintained", -- one of "all", "maintained" (parsers with maintainers), or a list of languages
  ignore_install = { "javascript" }, -- List of parsers to ignore installing
  highlight = {
    enable = true,              -- false will disable the whole extension
    -- disable = { "c", "rust" },  -- list of language that will be disabled
    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = false,
  },
  indent = {
    -- enable = true,
  }
}

-- Setup nvim autopairs
require('nvim-autopairs').setup({
  disable_filetype = { "TelescopePrompt" },
  map_cr = true, --  map <CR> on insert mode
  map_complete = true, -- it will auto insert `(` (map_char) after select function or method item
  auto_select = false,  -- auto select first item
  map_char = { -- modifies the function or method delimiter by filetypes
    all = '(',
    tex = '{'
  }
})

-- If you want insert `(` after select function or method item
local cmp_autopairs = require('nvim-autopairs.completion.cmp')
local cmp = require('cmp')
cmp.event:on( 'confirm_done', cmp_autopairs.on_confirm_done({  map_char = { tex = '' } }))

lsp_echo = function(entry)
  print(entry:get_completion_item().label)
end

cmp.event:on( 'confirm_done', lsp_echo )


--
require('telescope').setup({})

require('cheatsheet').setup({})

-- Tree file browser icon pack
require'nvim-web-devicons'.setup({
  default = true;
})

-- Tree file browser setup
require'nvim-tree'.setup({
  view = {
    auto_resize = true,
  },
})

-- Top bar
require('bufferline').setup()

require('hop').setup({})

local wk = require('which-key')
wk.setup({})
wk.register({
	["<leader>"] = { name = "find (Telescope)" },
    ["<leader>f<leader>"] = { "<cmd>Telescope<cr>", "Telescope", noremap=true },
    ["<leader>ff"] = { "<cmd>Telescope find_files<cr>", "Telescope Find Files", noremap=true },
    ["<leader>fg"] = { "<cmd>Telescope live_grep<cr>", "Telescope Live Grep", noremap=true },
    ["<leader>fb"] = { "<cmd>Telescope buffers<cr>", "Telescope Buffers", noremap=true },
    ["<leader>ft"] = { "<cmd>Telescope tags<cr>", "Telescope Tags", noremap=true },
    ["<leader>fs"] = { "<cmd>Telescope symbols<cr>", "Telescope Symbols", noremap=true},
    ["<leader>fh"] = { "<cmd>Telescope help_tags<cr>", "Telescope Help Tags", noremap=true },
})

wk.register({
  ["<leader><leader>"] = { name = "Hop (easymotion)" },
  ["<leader><leader>w"] = { "<cmd>HopWord<cr>", "HopWord", noremap=true },
  ["<leader><leader>/"] = { "<cmd>HopPattern<cr>", "HopPattern", noremap=true },
  ["<leader><leader>f"] = { "<cmd>HopChar1<cr>", "HopChar1", noremap=true },
  ["<leader><leader>l"] = { "<cmd>HopLine<cr>", "HopLine", noremap=true },
})

EOF


" Use <Tab> and <S-Tab> to navigate through popup menu
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" use <Tab> as trigger keys
" imap <Tab> <Plug>(completion_smart_tab)
" imap <S-Tab> <Plug>(completion_smart_s_tab)

" Code navigation shortcuts
nnoremap <silent> <c-]> <cmd>lua vim.lsp.buf.definition()<CR>
nnoremap <silent> K     <cmd>lua vim.lsp.buf.hover()<CR>
nnoremap <silent> gD    <cmd>lua vim.lsp.buf.implementation()<CR>
nnoremap <silent> <c-k> <cmd>lua vim.lsp.buf.signature_help()<CR>
nnoremap <silent> 1gD   <cmd>lua vim.lsp.buf.type_definition()<CR>
nnoremap <silent> gr    <cmd>lua vim.lsp.buf.references()<CR>
nnoremap <silent> g0    <cmd>lua vim.lsp.buf.document_symbol()<CR>
nnoremap <silent> gW    <cmd>lua vim.lsp.buf.workspace_symbol()<CR>
nnoremap <silent> gd    <cmd>lua vim.lsp.buf.declaration()<CR>
nnoremap <silent> ga    <cmd>lua vim.lsp.buf.code_action()<CR>

" Set updatetime for CursorHold
" 800ms of no cursor movement to trigger CursorHold
" set updatetime=800
" Show diagnostic popup on cursor hold
" autocmd CursorHold * lua vim.lsp.diagnostic.show_line_diagnostics()

" Goto previous/next diagnostic warning/error
nnoremap <silent> g[ <cmd>lua vim.lsp.diagnostic.goto_prev()<CR>
nnoremap <silent> g] <cmd>lua vim.lsp.diagnostic.goto_next()<CR>

" have a fixed column for the diagnostics to appear in
" this removes the jitter when warnings/errors flow in
set signcolumn=yes

" Enable type inlay hints
autocmd CursorMoved,InsertLeave,BufEnter,BufWinEnter,TabEnter,BufWritePost *
\ lua require'lsp_extensions'.inlay_hints{ prefix = '', highlight = "Comment", enabled = {"TypeHint", "ChainingHint", "ParameterHint"} }

" Find files using Telescope command-line sugar.
" nnoremap <leader>ff <cmd>Telescope find_files<cr>
" nnoremap <leader>fg <cmd>Telescope live_grep<cr>
" nnoremap <leader>fb <cmd>Telescope buffers<cr>
" nnoremap <leader>ft <cmd>Telescope help_tags<cr>

" EasyNavigation
" nnoremap <leader><leader>w <cmd>HopWord<CR>
" nnoremap <leader><leader>/ <cmd>HopPattern<CR>
" nnoremap <leader><leader>f <cmd>HopChar1<CR>
" nnoremap <leader><leader>l <cmd>HopLine<CR>

" a basic set up for LanguageClient-Neovim
" << LSP >> {{{
" let g:LanguageClient_autoStart = 0
" nnoremap <leader>lcs :LanguageClientStart<CR>" if you want it to turn on automatically
" let g:LanguageClient_autoStart = 1
" let g:LanguageClient_serverCommands = {
"     \ 'python': ['pyls'],
"     \ 'rust': ['rust-analyzer'],
"     \ 'go': ['go-langserver'],
"     \ 'c': ['clangd'] }
" noremap <silent> K :call LanguageClient#textDocument_hover()<CR>
" noremap <leader>R :call LanguageClient#textDocument_rename()<CR>
" noremap <leader>gr :call LanguageClient#textDocument_references()<CR>
" noremap <leader>gt :call LanguageClient#textDocument_typeDefinition()<CR>
" noremap <leader>gi :call LanguageClient#textDocument_implementation()<CR>
" noremap <leader>gd :call LanguageClient#textDocument_definition()<CR>
" noremap <leader>gs :call LanguageClient#textDocument_documentSymbol()<CR>
" noremap <leader>ge :call LanguageClient#textDocument_explainErrorAtPoint()<CR>
" nmap <F5> <Plug>(lcn-menu)
" "nmap <leader> gd <Plug>(lcn-definition)
" " }}}
" 
" " Fixing LanguageClient#textDocument_hover issue with md not rendering
" " https://github.com/autozimu/LanguageClient-neovim/issues/921
" augroup markdown_language_client_commands
"     autocmd!
"     autocmd WinLeave __LanguageClient__ ++nested call <SID>fixLanguageClientHover()
" augroup END
" 
" function! s:fixLanguageClientHover()
"     setlocal modifiable
"     setlocal conceallevel=2
"     setlocal nonu
"     setlocal nornu
"     normal i
"     setlocal nomodifiable
" endfunction

" Colors
let base16colorspace=256  " Access colors present in 256 colorspace
colorscheme base16-tomorrow-night

let g:lightline = {
      \ 'colorscheme': 'base16_tomorrow_night',
      \ }

" Disable `--INSERT` in cmdline since we have lightline
set noshowmode

"=============================================================


" Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
" If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
" (see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
if (empty($TMUX))
  if (has("nvim"))
    "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
  "Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
  if (has("termguicolors"))
    set termguicolors
  endif
endif

" Enable true color 启用终端24位色
if exists('+termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif


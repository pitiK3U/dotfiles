# README
To install cloned dotfiles into your system use `install.sh`.
To install dotfiles remotely into your system:
```bash
wget https://gitlab.com/pitiK3U/dotfiles/install-remote.sh
chmod +x install-remote.sh
./install-remote.sh
```

## How to use 
My idea of using dotfiles is that they *should* consist of three parts (files):
Original file, my file, local file. For example: `.bashrc`, `.bashrc.user` and `.bashrc.local`.
`.bashrc` - default file generated from the system/distro.
`.bashrc.user` - file shared across many systems with shared configurations.
`.bashrc.local` - file that changes local configurations, used for special setting for certain machine.

## Apps

### Used themes
- base16-seti
- base16-tomorrow-night
<!-- markdown cheats -->
- base16-atelier-dune
- base16-atelier-lakeside
- base16-pop
- base16-woodland

### Editors/IDEs
- vim/neovim
- atom
- vscode
- itellij

### Terminal
- [st](https://st.suckless.org/)
- [Alacritty](https://github.com/alacritty/alacritty)
- [Konsole](https://konsole.kde.org/)

### Markdown editor
- [EME](https://github.com/egoist/eme)
- [Apostrophe](https://somas.pages.gitlab.gnome.org/apostrophe)
- [Mark Text](https://github.com/marktext/marktext)

### Fonts
- Fira Code
- Iosevka
- Noto mono

### Cli

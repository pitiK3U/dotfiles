# .bashrc

# Set color shell for nvim scheme
TERM=xterm-256color

# Enable fzf
[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# Terminal prompt ========================
# Cool color upgrade: https://gist.github.com/justintv/168835#gistcomment-3168491
function color_my_prompt {
# Colors need to be wrapped in \[ \] to elimate those chars in cmdline
# See: https://www.gnu.org/software/bash/manual/bash.html#Controlling-the-Prompt
    local rs="\[\e[0m\]"
    local green="\[\e[38;2;181;189;104m\]"
    local blue="\[\e[38;2;129;162;190m\]"
    local red="\[\e[38;2;204;102;102m\]"

    local __user_and_host="$green\u$rs@$blue\h$rs"
    local __cur_dir="$red\w$rs"

    # FIX: branch var is set incorrectly
    local remote=$(git remote 2> /dev/null | head -n 1 | sed -e 's/\(.*\)/\1:/')
	local branch=$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
    local __git=$(echo "\[\e[36m\]$remote\[\e[33m\]$branch\[\e[0m\]" | sed -e 's/\(.*\):\(.*\)/(\1\\[\o33[0m\\]:\2)/')

    PS1="$__user_and_host $__cur_dir $__git\$ "
}
# configure PROMPT_COMMAND which is executed each time before PS1
export PROMPT_COMMAND=color_my_prompt
export PS2="▶ "

# git
# TODO: set alias -> git log --oneline --abbrev -n 16

# ========================================
# Set colorful manpager
export MANPAGER="less -R --use-color -Dd+g -Du+b"

# Make show options on first tab
bind 'set show-all-if-ambiguous on'
# Cycle through options on first tab
# bind 'TAB:menu-complete'

# switch_layout() {
#     # if current keyboard is not `us` switch to us
#     if ! setxkbmap -v 2>&1 | grep -q "+us+"
#     then
#         setxkbmap us
#     else
#         setxkbmap cz -variant qwerty_bksl
#     fi
# }

# Set key `Shift+f11` to call `switch_layout`
# bind -x '"\e[23;2~":switch_layout'

# ========================================

# git clone https://github.com/chriskempson/base16-shell.git ~/.config/base16-shell
# Base16 Shell
BASE16_SHELL="$HOME/.config/base16-shell/"
[ -n "$PS1" ] && \
    [ -s "$BASE16_SHELL/profile_helper.sh" ] && \
        eval "$("$BASE16_SHELL/profile_helper.sh")"

" Save on :W
command! W w

""""""""""""""""""""""""""""""""""""
" Line
""""""""""""""""""""""""""""""""""""
" Show relative number
set number
set relativenumber

"""""""""""""""""""""""""""""""""""""
" Indents
"""""""""""""""""""""""""""""""""""""
" replace tabs with spaces
set expandtab
" 1 tab = 4 spaces
set softtabstop=4 shiftwidth=4

" list whitespace characters
" set list

" set display chars for whitespace
" set listchars=tab:>\ ,trail:-
set listchars=tab:»\ 
set listchars+=extends:›,precedes:‹
set listchars+=nbsp:␣
set listchars+=trail:▪ "•
set listchars+=lead:·

" when deleting whitespace at the beginning of a line, delete
" 1 tab worth of spaces (for us this is 4 spaces)
set smarttab

" when creating a new line, copy the indentation from the line above
set autoindent
" does the right thing (mostly) in programs
set smartindent

" better backspace support for indent etc
set backspace=indent,eol,start

" stricter indent for C programs
set cindent

"""""""""""""""""""""""""""""""""""""
" Search
"""""""""""""""""""""""""""""""""""""
" Ignore case when searching
set ignorecase
set smartcase

" highlight search results (after pressing Enter)
set hlsearch

" highlight all pattern matches WHILE typing the pattern
set incsearch

"""""""""""""""""""""""""""""""""""""
" Mix
"""""""""""""""""""""""""""""""""""""
" show commandline menu
set wildmenu
" When command completion is issued, command is completed the longest part it
" has common with different one and menu is showed
set wildmode=longest,full

" show the mathing brackets
set showmatch

" enable mouse
set mouse=a

" detect file changes and update
set autoread

" disable random aligning on paste
" FIX: this disables indentation on <Enter>
" set paste

" Set syntax highlighing as default
syntax enable

" Always see status line
set laststatus=2

" Enable type recognition
filetype plugin indent on

" 
set history=1000

"""""""""""""""""""""""""""""""""""""
" Navigation
"""""""""""""""""""""""""""""""""""""
map [[ ?{<CR>w99[{
map ][ /}<CR>b99]}
map ]] j0[[%/{<CR>
map [] k$][%?}<CR>

"""""""""""""""""""""""""""""""""""""
" Colors
"""""""""""""""""""""""""""""""""""""
" Base16 shell 
if "$BASE16_THEME" ==# "tomorrow-night"
    if filereadable(expand("~/.dotfiles/.vimrc_background"))
      let base16colorspace=256
      source ~/.dotfiles/.vimrc_background
    endif

" source ~/.dotfiles/.vim/colors/base16-tomorrow-night.vim
" let base16colorspace=256
" colorscheme base16-tomorrow-night 

else
    colorscheme desert
endif
